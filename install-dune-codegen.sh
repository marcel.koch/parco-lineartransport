#! /bin/bash

declare -A required_modules=(["core"]="dune-common
                                       dune-grid
                                       dune-localfunctions
                                       dune-istl
                                       dune-geometry"
                             ["staging"]="dune-typetree
                                          dune-functions
                                          dune-uggrid"
                             ["extensions"]="dune-alugrid"
                             ["quality"]="dune-testtools"
                             ["pdelab"]="dune-pdelab"
                            )

for group in "${!required_modules[@]}"
do
    for module in ${required_modules[$group]}
    do
        git clone "https://gitlab.dune-project.org/$group/$module.git"
    done
done

git clone --recursive "https://gitlab.dune-project.org/extensions/dune-codegen.git"

#alias dunecontrol=./dune-common/bin/dunecontrol

#dunecontrol --opts=erlangen.opts all
