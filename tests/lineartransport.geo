// Gmsh project created on Mon Jul 22 10:57:57 2019
SetFactory("OpenCASCADE");

//+
Point(1) = {0, 0, 0, 0.05};
//+
Point(2) = {1, 0, 0, 0.05};
//+
Point(3) = {0, 1, 0, 0.05};
//+
Point(4) = {1, 1, 0, 0.05};
//+
SetFactory("OpenCASCADE");
//+
SetFactory("OpenCASCADE");
//+
SetFactory("OpenCASCADE");
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 4};
//+
Line(3) = {4, 3};
//+
Line(4) = {3, 1};
//+
Line Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
