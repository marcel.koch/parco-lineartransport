#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <chrono>
#include "dune/common/parallel/mpihelper.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/alugrid/grid.hh"
#include "dune/grid/uggrid.hh"
#include "dune/pdelab/finiteelementmap/opbfem.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/constraints/p0.hh"
#include "dune/pdelab/constraints/p0ghost.hh"
#include "dune/testtools/gridconstruction.hh"
#include "lineartransport_rOperator_file.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/gridoperator/onestep.hh"
#include "dune/pdelab/gridoperator/fastdg.hh"
#include "lineartransport_massOperator_file.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/vtksequencewriter.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "dune/codegen/vtkpredicate.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/pdelab/common/functionutilities.hh"

template<typename Grid>
std::shared_ptr<Grid> structured_grid(const std::array<int, 2>& elements, bool split_diagonal){
  Dune::GridFactory<Grid> factory;
  std::array<int,2> vertices = elements;
  for( auto& vertices_per_dim: vertices )
    vertices_per_dim++;

  for (int j = 0; j < vertices[1]; ++j) {
    for (int i = 0; i < vertices[0]; ++i) {
      const Dune::FieldVector<double,2> pos{double(i) / vertices[0], double(j) / vertices[1]};
      factory.insertVertex(pos);
    }
  }

  std::vector<unsigned int> corners(3);
  for (int j = 0; j < elements[1]; ++j) {
    for (int i = 0; i < elements[0]; ++i) {
      // compute corner indices of lower triangle
      if(split_diagonal){
        corners[0] = i + 1 + vertices[0] * j;
        corners[1] = i + 1 + vertices[0] * (j + 1);
        corners[2] = i + vertices[0] * j;
      }
      else {
        corners[0] = i + vertices[0] * j;
        corners[1] = i + 1 + vertices[0] * j;
        corners[2] = i + vertices[0] * (j + 1);
      }
      factory.insertElement(Dune::GeometryTypes::triangle, corners);

      // compute corner indices of upper triangle
      if(split_diagonal){
        corners[0] = i + vertices[0] * (j + 1);
        corners[1] = i + vertices[0] * j;
        corners[2] = i + 1 + vertices[0] * (j + 1);
      }
      else {
        corners[0] = i + 1 + vertices[0] * (j + 1);
        corners[1] = i + vertices[0] * (j + 1);
        corners[2] = i + 1 + vertices[0] * j;
      }
      factory.insertElement(Dune::GeometryTypes::triangle, corners);
    }
  }
  std::cout << "FINISHED CONSTRUCTION" << std::endl;
  return std::shared_ptr<Grid>(factory.createGrid().release());
}


int main(int argc, char** argv){
  try
  {    

    // Initialize mpi and read options from ini file
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    Dune::ParameterTree initree;
    Dune::ParameterTreeParser::readINITree(argv[1], initree);
    using RangeType = double;
    double time = 0.0;
    
    // Set up grid (view)
    using Grid = Dune::UGGrid<2>;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    std::shared_ptr<Grid> grid = structured_grid<Grid>(initree.get<std::array<int, 2>>("elements"), true);
    GV gv = grid->leafGridView();

    // Set up finite element maps
    using DG1_FEM = Dune::PDELab::OPBLocalFiniteElementMap<DF, RangeType, 1, 2, Dune::GeometryType::simplex>;
    DG1_FEM dg1_fem;
    
    // Set up grid function spaces
    using VB = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed, 3>;
    using ConstraintsAssember = Dune::PDELab::P0ParallelGhostConstraints;
    using GFS = Dune::PDELab::GridFunctionSpace<GV, DG1_FEM, ConstraintsAssember, VB>;
    GFS gfs(gv, dg1_fem);
    gfs.name("gfs");
    
    // Set up constraints container
    using CC = GFS::ConstraintsContainer<RangeType>::Type;
    CC cc{};
    cc.clear();
    Dune::PDELab::constraints(gfs, cc);

    // Set up grid grid operators
    using LOP_R = rOperator<GFS, GFS, RangeType>;
    LOP_R lop_r(gfs, gfs, initree);
    using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
    using GO_r = Dune::PDELab::FastDGGridOperator<GFS, GFS, LOP_R, MatrixBackend, DF, RangeType, RangeType, CC, CC>;
    using LOP_MASS = massOperator<GFS, GFS, RangeType>;
    using GO_mass = Dune::PDELab::FastDGGridOperator<GFS, GFS, LOP_MASS, MatrixBackend, DF, RangeType, RangeType, CC, CC>;
    using IGO = Dune::PDELab::OneStepGridOperator<GO_r,GO_mass,false>;
    gfs.update();
    int generic_dof_estimate =  6 * gfs.maxLocalSize();
    int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
    MatrixBackend mb(dofestimate);
    GO_r go_r(gfs, cc, gfs, cc, lop_r, mb);
    LOP_MASS lop_mass(gfs, gfs, initree);
    GO_mass go_mass(gfs, cc, gfs, cc, lop_mass, mb);
    IGO igo(go_r, go_mass);
    {
      auto gfs_size = gv.comm().sum(gfs.size());
      auto cc_size = gv.comm().sum(cc.size());
      if (gv.comm().rank() == 0) {
        std::cout << "gfs with " << gfs_size << " dofs generated  " << std::endl;
        std::cout << "cc with " << cc_size << " dofs generated  " << std::endl;
      }
    }
    
    // Set up solution vectors
    using V_R = Dune::PDELab::Backend::Vector<GFS,DF>;
    V_R x_r(gfs);
    x_r = 0.0;
    auto initialValue = Dune::PDELab::makeInstationaryGridFunctionFromCallable(gv, [&](const auto& is, const auto& xl){
      auto x=is.geometry().global(xl);
      using std::min;
      return 0.25 * (cos(3.141592653589793 * min(1.0, sqrt((x[1] - 0.25) * (x[1] - 0.25) + (x[0] - 0.25) * (x[0] - 0.25)) / 0.15)) + 1);

    }, lop_r);
    Dune::PDELab::interpolate(initialValue, gfs, x_r);

    // Set up linear solvers
    using LinearSolver = Dune::PDELab::ISTLBackend_OVLP_ExplicitDiagonal<GFS>;
    LinearSolver ls(gfs);
    
    // Set up visualization
    using VTKSW = Dune::VTKSequenceWriter<GV>;
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    Dune::RefinementIntervals subint(initree.get<int>("vtk.subsamplinglevel", 1));
    VTKWriter vtkwriter(gv, subint);
    VTKSW vtkSequenceWriter(std::make_shared<VTKWriter>(vtkwriter), "lineartransport", "output", "");
    CuttingPredicate predicate;
    Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter, gfs, x_r, Dune::PDELab::vtk::defaultNameScheme(), predicate);
    vtkSequenceWriter.write(time, Dune::VTK::appendedraw);
    
    // Set up time stepping method
    using EOSM = Dune::PDELab::ExplicitOneStepMethod<RangeType, IGO, LinearSolver, V_R>;
    using TSM = Dune::PDELab::Shu3Parameter<RangeType>;
    TSM tsm;
    EOSM eosm(tsm, igo, ls);
    eosm.setVerbosityLevel(0);

    // Compute minimal inner radius
    auto r = std::numeric_limits<double>::max();
    for(const auto& element: elements(gv)){
      const auto& geo = element.geometry();
      const auto center = geo.center();

      for (int i = 0; i < 3; ++i) {
        const auto corner = geo.corner(i);
        r = std::min(r, (corner - center).two_norm());
      }
    }
    r = gv.comm().min(r);
    if(gv.comm().rank() == 0)
      std::cout << r << std::endl;

    // Use dt from ini file if possible, otherwise choose dt < 0.15 * h/||beta||
    const auto dt = initree.get<double>("instat.dt", 0.15 * 2 * r / sqrt(2.));
    const auto T = initree.get<double>("instat.T", 1.0);
    int step_number(0);
    int output_every_nth = initree.get<int>("instat.output_every_nth", 1);


    if(gv.comm().rank() == 0)
      std::cout << dt << std::endl;

    gv.comm().barrier();

    auto start = std::chrono::high_resolution_clock::now();

    while (time<T-1e-8){
      // Assemble constraints for new time step
      lop_r.setTime(time+dt);

      // Do time step
      V_R x_rnew(x_r);
      eosm.apply(time, dt, x_r, x_rnew);

      // Accept new time step
      x_r = x_rnew;
      time += dt;

      step_number += 1;
      if (step_number%output_every_nth == 0){
        // Output to VTK File
        vtkSequenceWriter.vtkWriter()->clear();
        Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter, gfs, x_r,
                                             Dune::PDELab::vtk::defaultNameScheme(), predicate);
        vtkSequenceWriter.write(time, Dune::VTK::appendedraw);
      }
    }

    auto end = std::chrono::high_resolution_clock::now();
    gv.comm().barrier();

    auto diff = gv.comm().max(std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count());
    if(gv.comm().rank() == 0)
      std::cout << diff << std::endl;

    // Calculate error of approximate solution
    auto exactSolution = Dune::PDELab::makeInstationaryGridFunctionFromCallable(gv, [&](const auto& x){
      using std::min;
      return 0.25 * (cos(3.141592653589793 * min(1.0, sqrt((x[1] - 0.75) * (x[1] - 0.75) + (x[0] - 0.75) * (x[0] - 0.75)) / 0.15)) + 1);
    }, lop_r);

    using DGF = Dune::PDELab::DiscreteGridFunction<decltype(gfs),decltype(x_r)>;
    DGF dgf(gfs,x_r);
    using DifferenceSquaredAdapter = Dune::PDELab::DifferenceSquaredAdapter<decltype(exactSolution), decltype(dgf)>;
    DifferenceSquaredAdapter dsa(exactSolution, dgf);

    typename decltype(dsa)::Traits::RangeType err(0.0);
    Dune::PDELab::integrateGridFunction(dsa, err, 10);

    auto l2error = sqrt(gv.comm().sum(err));
    if (gv.comm().rank() == 0){
      std::cout << "L2 Error : " << l2error << std::endl;
    }
  }
  catch (Dune::Exception& e)
  {    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }  
  catch (std::exception& e)
  {    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }  
}

